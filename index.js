const canvas = document.querySelector('canvas');
const context = canvas.getContext('2d');

// runs new gen
//stops new gen
//saves current value of new gen

const buttons = document.getElementsByClassName('buttons');
console.log(buttons);


const resolution = 10;
let width = 800;
let height = 500;

canvas.width = width;
canvas.height = height;

const COLS = canvas.width / resolution;
const ROWS = canvas.height / resolution;

function seedGrid() {
    return new Array(COLS).fill(null)
    .map(() => new Array(ROWS).fill(null)
        .map(() => Math.floor(Math.random() * 2)));
}

let grid = seedGrid();
requestAnimationFrame(update);

function update() {
  grid = newGen(grid);
  render(grid);
  requestAnimationFrame(update);
}

function newGen(grid) {
    const newGen = grid.map(arr => [...arr]);

    for (let col = 0; col < grid.length; col++) {
      for (let row = 0; row < grid[col].length; row++) {
        const cell = grid[col][row];
        let totalNeighbours = 0;
        for (let i = -1; i < 2; i++) {
          for (let j = -1; j < 2; j++) {
            if (i === 0 && j === 0) {
              continue;
            }
            const cell_x = col + i;
            const cell_y = row + j;
  
            if (cell_x >= 0 && cell_y >= 0 && cell_x < COLS && cell_y < ROWS) {
              const currentNeighbour = grid[col + i][row + j];
              totalNeighbours += currentNeighbour;
            }
          }
        }
  
        // rules
        if (cell === 1 && totalNeighbours < 2) {
          newGen[col][row] = 0;
        } else if (cell === 1 && totalNeighbours > 3) {
          newGen[col][row] = 0;
        } else if (cell === 0 && totalNeighbours === 3) {
          newGen[col][row] = 1;
        }
      }
    }
    return newGen;
}

function render(grid) {
    for (let col = 0; col < grid.length; col++) {
        for (let row = 0; row < grid[col].length; row++) {
            const cell = grid[col][row];
            
            context.beginPath();
            context.rect(col * resolution, row * resolution, resolution, resolution);
            context.fillStyle = cell ? "black" : "white";
            context.fill();
            //context.stroke();
        }
    }
}