# GAME OF LIFE

This project was bootstrapped with simple vanilla Javascript.

## Clone the repository

Use `git clone https://Charles_Kiarie@bitbucket.org/Charles_Kiarie/gameoflife.git`

Run the app in or with a live server.\

Run the app by opening index.html in your browser.\

## Technologies used:
- Javascript
- Html5 & css3

## Notes & Considerations

I did not finish it yet. Still need to add the following.

### TO-DO

- [ ] A way to configure the size of the canvas from user inputs.
- [ ] A way to seed a single world instance.
- [ ] A way to start the simulation from a single world instance.
- [ ] A way to pause and or stop the simulation without exiting the app.
- [ ] A way to save a seed and load it in for simulation.

I have attached a screenshot of the user interface below.

## Screenshots

![App screenshots](https://drive.google.com/uc?export=view&id=1DI_PvJ48F5sTj3Da_4Fm_b11v_FBpXqi)